import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { EntityID } from 'src/app/bussiness/entity/entityid';
import { Faculte } from 'src/app/bussiness/entity/shool';
import { FirebaseApi } from '../vendors/google-api/firebase/FirebaseApi';
import { ResultStatut } from '../vendors/google-api/resultstatut';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
  })
export class SchoolService
{
    faculteSubject:Subject<Faculte[]>=new Subject<Faculte[]>();
    filierSubject:Subject<String[]>=new Subject<String[]>();
    niveauSubject:Subject<String[]>=new Subject<String[]>();

    faculteList:Map<EntityID,Faculte>=new Map<EntityID,Faculte>();


    constructor(private apiService:ApiService){
        this.getFaculteFromApi();
    };

    addFaculte(faculte:Faculte):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
            this.apiService.post("faculte/new",faculte.toString())
            .subscribe((result)=>{
                if(result)
                {
                    this.faculteList.set(faculte.id,faculte);
                    this.emitFaculte();
                    resolve(new ResultStatut());
                }
                let resultat:ResultStatut=new ResultStatut();
                resultat.message=result;
                reject(resultat);
            },(error)=> {
                let resultat:ResultStatut=new ResultStatut();
                resultat.apiCode=ResultStatut.UNKNOW_ERROR;
                resultat.message=error;
                reject(resultat);
            })
        })
    }
    addFiliere(faculte:String,filiere:String):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
            
        })
    }
    addNiveauToFieliere(faculte:String,filiere:String,niveau:String):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{

        })
    }

    addCours(faculte:String,filiere:String,niveau:number,cours:{idProf:EntityID,idCours:EntityID}):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
           
        })
    }

    getSchool():Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
           
        })
    }

    selectFilier(faculteID:EntityID,filiereID:EntityID)
    {
        if(!this.faculteList.has(faculteID))
        {
            this.emitFaculte([]);
            return;
        }
        let f:Faculte=this.faculteList.get(faculteID);

    }
    selectNiveau(faculteID:EntityID,filiereID:EntityID,niveau:number)
    {

    }
    getFaculteList()
    {
        let fl=[];
        for(let f of this.faculteList.keys())
        {
            fl.push(this.faculteList.get(f));
        }
        return fl;
    }
    getFaculteFromApi():Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
            this.apiService.get("faculte/list")
            .subscribe((result)=>{
                if(result && result.statut==ResultStatut.SUCCESS)
                {
                    result.result.forEach((faculte)=>{
                        let id:EntityID = new EntityID();
                        id.setId(faculte.id);
                        let f=new Faculte(id);
                        f.hydrate(faculte);
                        this.faculteList.set(id,f);
                    });
                    this.emitFaculte();
                    return;

                }
                reject(result);
            },(error)=>reject(error))
        })
    }
    emitFaculte(fList=this.getFaculteList())
    {
        this.faculteSubject.next(fList);
    }
    emitFiliere(fiList)
    {
        this.filierSubject.next(fiList);
    }
    
}