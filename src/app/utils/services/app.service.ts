import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/bussiness/entity/user';
import { ResultStatut } from '../vendors/google-api/resultstatut';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public user = {
    firstName: 'Alexander',
    lastName: 'Pierce',
    image: 'assets/img/user2-160x160.jpg',
  };

  constructor(private router: Router, private authService:AuthService) {}

  login(user:User):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>{{
      this.authService.signIn(user)
      .then((result:ResultStatut)=> resolve(result))
      .catch((error:ResultStatut)=>{
        reject(error);
      })
    }})
  }

  register() {
    localStorage.setItem('token', 'LOGGED_IN');
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
