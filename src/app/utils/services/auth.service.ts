import { Injectable, OnDestroy } from '@angular/core';
import { User } from 'src/app/bussiness/entity/user';
import { FirebaseApi } from '../vendors/google-api/firebase/FirebaseApi';
import { FirebaseConstant } from '../vendors/google-api/firebase/FirebaseConstant';
import { ResultStatut } from '../vendors/google-api/resultstatut';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:User=null;
  isAuth:boolean=false;
  constructor(private fetchApi:FirebaseApi) {}

  signIn(userN:User):Promise<ResultStatut>
  {
    let action=new ResultStatut();
    return new Promise<ResultStatut>((resolve,reject)=>{
      this.fetchApi.signInApi(userN.email,userN.password)
      .then(result=>{
        this.isAuth=true;
        action=result;
        resolve(result)
      })
      .catch(result=>{
        this.fetchApi.handleApiError(result);
        reject(result);
      })       
    });
  }

  signOut():void
  {
    this.user=null;
    this.isAuth=false;
    this.fetchApi.signOutApi();
  }
  isConnected():boolean
  {
    // this.getUserDataFromStorage();
    return this.isAuth && this.user!=null && this.user!=undefined;
  }
  getUser():User
  {
    return this.user;
  }
  signInNewUser(user:User)
  {
    // return new Promise<ResultStatut>((resolve,reject)=>
    // {
    //   this.manageAccount.createAccount(user)
    //   .then((result)=>{
    //     return this.signIn(user,false);
    //   })
    //   .then((result)=>{
    //     this.user=User.fromObject({
    //       email:user.email,
    //       name:user.name,
    //       id:this.fetchApi.user.uid,
    //       disponibilite:{}
    //     });
    //     return this.manageAccount.saveAccount(this.user)
    //   })
    //   .then((e)=>{        
    //     this.isAuth=true;
    //     return this.manageAccount.getUsersFromApi();
    //   })
    //   .then((result)=>{
    //     // this.setUserDataToStorage();
    //     resolve(result);
    //   })
    //   .catch(e=>
    //   {
    //     this.handleApiError(e);
    //     reject(e)
    //   })
    // });
  }
}
