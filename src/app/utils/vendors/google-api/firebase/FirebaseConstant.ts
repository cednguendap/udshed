
 export class FirebaseConstant {
  static AUTH_WRONG_PASSWORD="auth/wrong-password";
  static AUTH_EMAIL_ALREADY_USE="auth/email-already-in-use";
  static AUTH_WEAK_PASSWORD="auth/weak-password";
  static NET_NETWORK_FAIL="auth/network-request-failed";
  static STORAGE_OBJECT_NOT_FOUND="storage/object-not-found";
  static DATABASE_DISCONNECTED=-4;
  static DATABASE_NETWORK_ERROR=-24;
  static DATABASE_OPERATION_FAILED=-2;
  static DATABASE_PERMISSION_DENIED=-3;
  static DATABASE_SERVICE_UNAVAILABLE=-10;
  static DATABASE_UNKNOW_ERROR=-999;
}
