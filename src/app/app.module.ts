import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './pages/main/header/header.component';
import { FooterComponent } from './pages/main/footer/footer.component';
import { MenuSidebarComponent } from './pages/main/menu-sidebar/menu-sidebar.component';
import { BlankComponent } from './views/blank/blank.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './views/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { MessagesDropdownMenuComponent } from './pages/main/header/messages-dropdown-menu/messages-dropdown-menu.component';
import { NotificationsDropdownMenuComponent } from './pages/main/header/notifications-dropdown-menu/notifications-dropdown-menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppButtonComponent } from './components/app-button/app-button.component';

import { CommonModule, registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import { UserDropdownMenuComponent } from './pages/main/header/user-dropdown-menu/user-dropdown-menu.component';
import { RouterModule } from '@angular/router';
import { ContentHeadersComponent } from './components/content-headers/content-headers.component';
import { EmploitempsComponent } from './views/emploitemps/emploitemps.component';
import { 
  ScheduleModule ,
  AgendaService, 
  DayService,
  MonthAgendaService,
  MonthService,
  TimelineMonthService, 
  TimelineViewsService,
  WeekService, 
  WorkWeekService
} from '@syncfusion/ej2-angular-schedule';
import { EnseignantComponent } from './views/enseignant/enseignant.component';
import { FaculteComponent } from './views/faculte/faculte.component';
import { HttpClientModule } from '@angular/common/http';

registerLocaleData(localeEn, 'en-EN');

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    MenuSidebarComponent,
    BlankComponent,
    ProfileComponent,
    RegisterComponent,
    DashboardComponent,
    MessagesDropdownMenuComponent,
    NotificationsDropdownMenuComponent,
    AppButtonComponent,
    UserDropdownMenuComponent,
    ContentHeadersComponent,
    EmploitempsComponent,
    EnseignantComponent,
    FaculteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgbModule,
    CommonModule,
    RouterModule,
    ScheduleModule,
    HttpClientModule
  ],
  providers: [
    DayService, 
    WeekService, 
    MonthService, 
    TimelineViewsService, 
    TimelineMonthService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
