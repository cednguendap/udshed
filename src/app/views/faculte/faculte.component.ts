import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EntityID } from 'src/app/bussiness/entity/entityid';
import { Faculte } from 'src/app/bussiness/entity/shool';
import { SchoolService } from 'src/app/utils/services/school.service';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';

declare var $:any;

@Component({
  selector: 'app-faculte',
  templateUrl: './faculte.component.html',
  styleUrls: ['./faculte.component.scss']
})
export class FaculteComponent implements OnInit {
  form:FormGroup;
  isSubmitted:boolean=false;
  listFaculte:Faculte[]=[];
  path:any=[
    {
      'link':'/admin',
      'text':'Home'
    },
    {
      'link':'#',
      'text':'Faculte'
    },
  ]
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private schoolService:SchoolService,
    private toastService:ToastrService
  ) { }

  ngOnInit(): void {
      // $(".table").DataTable();
      $('#addFaculte').on('shown.bs.modal', function () {
        //$('#myInput').trigger('focus')
      });

      this.form=new FormGroup({
        nom: new FormControl("",[Validators.required]),
        description:new FormControl("")
      });
      this.schoolService.faculteSubject.subscribe((faculteList)=> this.listFaculte=faculteList)
  }

  addFaculte()
  {
    if(!this.form.valid)
    {
      document.querySelector('#inputNomFaculte').classList.add('is-invalid');
      return;
    }
    this.isSubmitted=true;
    let f:Faculte=new Faculte(new EntityID());
    f.hydrate(this.form.value);
    this.schoolService.addFaculte(f)
    .then((result:ResultStatut)=>{
      this.isSubmitted=false;      
      this.form.reset();
      document.querySelector('#inputNomFaculte').classList.remove('is-invalid');
      $('#addFaculte').modal('hide');
      this.toastService.success("Ajout de faculté avec success");
    })
    .catch((error:ResultStatut)=>{
      this.isSubmitted=false;      
      this.toastService.error(`<b>Erreur</b> ${error.message}`,"Ajout de faculté ");
    })
  }

}
