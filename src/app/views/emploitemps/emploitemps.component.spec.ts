import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploitempsComponent } from './emploitemps.component';

describe('EmploitempsComponent', () => {
  let component: EmploitempsComponent;
  let fixture: ComponentFixture<EmploitempsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmploitempsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploitempsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
