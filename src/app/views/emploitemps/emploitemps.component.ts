import { Component, OnInit } from '@angular/core';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService } from '@syncfusion/ej2-angular-schedule';

@Component({
  selector: 'app-emploitemps',
  templateUrl: './emploitemps.component.html',
  styleUrls: ['./emploitemps.component.scss'],
  providers: [DayService, WeekService, MonthService],
})
export class EmploitempsComponent implements OnInit {
  path:any=[
    {
      'link':'/admin',
      'text':'Home'
    },
    {
      'link':'#',
      'text':'Emploi de temps'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
