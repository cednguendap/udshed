import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enseignant',
  templateUrl: './enseignant.component.html',
  styleUrls: ['./enseignant.component.scss']
})
export class EnseignantComponent implements OnInit {
  path:any=[
    {
      'link':'/admin',
      'text':'Home'
    },
    {
      'link':'#',
      'text':'Emploi de temps'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
