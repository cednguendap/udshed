import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blank',
  templateUrl: './blank.component.html',
  styleUrls: ['./blank.component.scss'],
})
export class BlankComponent implements OnInit {
  path:any=[
    {
      'link':'/admin',
      'text':'Home'
    },
    {
      'link':'#',
      'text':'Blank Page'
    },
  ]
  constructor() {}

  ngOnInit() {}
}
