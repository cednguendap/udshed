import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentHeadersComponent } from './content-headers.component';

describe('ContentHeadersComponent', () => {
  let component: ContentHeadersComponent;
  let fixture: ComponentFixture<ContentHeadersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentHeadersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentHeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
