import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-headers.component.html',
  styleUrls: ['./content-headers.component.scss']
})
export class ContentHeadersComponent implements OnInit {
  @Input()path:any[]=[];
  @Input() title:String="";
  constructor() { }

  ngOnInit(): void {
  }

}
