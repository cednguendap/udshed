import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { AppService } from '../../utils/services/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/bussiness/entity/user';
import { EntityID } from 'src/app/bussiness/entity/entityid';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public isAuthLoading = false;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private appService: AppService,
    private router:Router
  ) {}

  ngOnInit() {
    // this.toastr.error('Hello world!', 'Toastr fun!');
    this.renderer.addClass(document.querySelector('app-root'), 'login-page');
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  login() {
    this.isAuthLoading=true;
    let user:User=new User((new EntityID()));
    user.hydrate(this.loginForm.value);

    if (this.loginForm.valid) {
      this.appService.login(user)
      .then((result:ResultStatut)=>{
        this.toastr.success("Success de l'authentification","",{
          timeOut:1000
        });
        setTimeout(() => {
          this.router.navigate(['/admin']);
        }, 110);
      })
      .catch((error:ResultStatut)=>{
        console.log(error);
        this.isAuthLoading=false;
        this.toastr.error('Login ou mot de passe invalide',"",{
          timeOut:1000,
          positionClass:'toast-top-right'
        });
        this.loginForm.controls.password.setValue("");
      })
    } else {
      this.toastr.error('Formulaire invalide');
      this.isAuthLoading=false;
    }
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.querySelector('app-root'), 'login-page');
  }
}
