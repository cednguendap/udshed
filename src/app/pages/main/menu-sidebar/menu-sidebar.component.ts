import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  Output,
  EventEmitter,
  ElementRef,
} from '@angular/core';
import { AppService } from 'src/app/utils/services/app.service';

@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.scss'],
})
export class MenuSidebarComponent implements OnInit, AfterViewInit {
  @ViewChild('mainSidebar', { static: false }) mainSidebar:ElementRef;
  @Output() mainSidebarHeight: EventEmitter<any> = new EventEmitter<any>();
  constructor(public appService: AppService,private sidebar:ElementRef) {}

  ngOnInit() {
    this.sidebar.nativeElement.querySelectorAll('li.dropdown')
    .forEach(element => {
      element.addEventListener("click",(e)=>{
        if(e.currentTarget.classList.contains('menu-open')) e.currentTarget.classList.remove("menu-open");
        else e.currentTarget.classList.add("menu-open");
      })
    });
  }

  ngAfterViewInit() {
    this.mainSidebarHeight.emit(this.mainSidebar.nativeElement.offsetHeight);
  }
}
