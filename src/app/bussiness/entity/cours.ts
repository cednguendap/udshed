import { Entity } from './entity';
import { EntityID } from './entityid';
import { Classe } from './shool';

export class Cours extends Entity
{
    intitule:String="";
    code:String="";
    idProf:EntityID=new EntityID();
    classes:String[]=[];
}