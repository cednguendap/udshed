import { Disponibilite } from './disponibility';
import { Entity } from './entity';
import { EntityID } from './entityid';

export class Faculte extends Entity
{
    nom:String="";
    description:String="";
    filiereList:Filiere[]=[];
    toString()
    {
        let r= {
            ...super.toString(),
            filieres:this.filiereList.map((fil:Filiere)=>fil.toString())
        }
        delete r["filiereList"];
        return r;
    }
    hydrate(obj:Record<string|number,any>)
    {
        super.hydrate(obj);
        if(obj.filieres) this.filiereList=obj.filieres.map((fil)=>{
            let f:Filiere=new Filiere(new EntityID());
            f.hydrate(fil);
            return f;
        })
    }

}

export class Filiere extends Entity
{
    nom:String="";
    description:String="";
    classesList:Classe[]=[];
    toString()
    {
        let r= {
            ...super.toString(),
            classes:this.classesList.map((classe:Classe)=>classe.toString())
        }
        delete r["classesList"];
        return r;
    }
    hydrate(obj:Record<string|number,any>)
    {
        super.hydrate(obj);
        if(obj.classes) this.classesList=obj.classes.map((classe)=>{
            let c:Classe=new Classe(new EntityID());
            c.hydrate(classe);
            return c;
        })
    }
}


export class Classe extends Entity
{
    niveau:number=1;
    calendrier:Disponibilite=null;

    toString()
    {
        let r={
            ...super.toString(),
            calendar:this.calendrier.toString()
        }
        delete r["calendrier"];
        return r;
    }

    hydrate(obj)
    {
        super.hydrate(obj);
        if(obj.calendrier) 
        {
            this.calendrier=new Disponibilite();
            this.calendrier.hydrate(obj.calendrier);
        }
    }
}