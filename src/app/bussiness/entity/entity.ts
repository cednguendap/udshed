import { idText } from "typescript";
import { EntityID } from "./entityid";

export abstract class Entity 
{
    /**
     * @description identifiant d'une entité
     * @type EntityID
     */
    public id:EntityID;
    
    
    constructor(id:EntityID)
    {
        this.id=id;
    }
    
    /**
     * @description Cette methode permet de verifier l'existance d'un valeur dans   
     *  un objet JSON afin de retourner sa valeur. cela évite des erreurs du a la tentative
     *  d'accés a un attribue non contenu dans l'objet JSON
     * @param object objet au format JSON
     * @param attr attribue dont on veu la valeur
     * @return null si l'attribut n'exite pas et sa valeur dans le cas contraire
     */
    purgeAttribute(object:Record<string|number,any>,attr:String):any
    {        
        if(object==null || object==undefined) return null;
        if(object.hasOwnProperty(attr.toString())) return object[attr.toString()]
        if(this.hasOwnProperty(attr.toString()))  return Reflect.get(this,attr.toString());
        return null;
    }

    /**
     * @inheritdoc
     */
    hydrate(entity:Record<string|number,any>):void
    {
        for(const key in entity)
        {
            if(this.hasOwnProperty(key.toString())) Reflect.set(this,key,entity[key]);
        }
    }


    /**
     * @inheritdoc
     */
    toString():any
    {
        let r:Record<string|number,any>={};
        for(const key in this )
        {
            let v=Reflect.get(this,key);
            if(typeof v.toString=='function') r[key]=v.toString();
            else r[key]=v;
        }   
        return r;
    }
    

}