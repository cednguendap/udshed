import { Disponibilite } from './disponibility';
import { User } from './user';

export class Teacher extends User
{
    dispo:Disponibilite=new Disponibilite();

    hydrate(obj:Record<string|number,any>):void
    {
        super.hydrate(obj);
        if(obj.hasOwnProperty('disponibility')) this.dispo.hydrate(obj.dispo);
    }
    toString()
    {
        return {
            ...super.toString(),
            disponibility:this.dispo.toString()
        }
    }
}