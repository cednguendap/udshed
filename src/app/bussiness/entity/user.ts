import { Disponibilite } from './disponibility';
import { Entity } from './entity';
import { EntityID } from './entityid';

export class User extends Entity{
    lname:string="";
    fname:String="";
    email:string="";
    password:string="";
    tel:string="";
    
    toString()
    {
        return {
            ...super.toString(),
            lname:this.lname,
            fname:this.fname,
            email:this.email,      
            tel:this.tel,      
        };
    }    
}
