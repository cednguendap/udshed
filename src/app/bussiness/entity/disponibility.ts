import { Entity } from './entity';
import { EntityID } from './entityid';

export class StatutPeriod{
    static available:boolean=true;
    static unavailable:boolean=false;
};
export class DispoPeriodTime extends Entity
{   
    period:string;
    status:StatutPeriod;
    cours:EntityID=null;
    teacher:EntityID=null;
    
    isAvailable():boolean
    {
        return this.status===StatutPeriod.available;
    }
    setStatut(statut:StatutPeriod):void{
        this.status=statut;
    }
    hydrate(obj)
    {
        super.hydrate(obj);
        if(obj.cours){
            this.cours=new EntityID();
            this.cours.setId(obj.cours)
        }
        if(obj.teacher)
        {
            this.teacher=new EntityID();
            this.teacher.setId(obj.teacher)
        }
    }
    toString():Record<string,any>
    {
        let obj= {
            period:this.period,
            statut:this.status
        }
        if(this.teacher) obj["teacher"]=this.teacher.toString();
        if(this.cours) obj["cours"]=this.cours.toString();
        return obj;
    }
}

export class DispoPeriodDays
{
    day:number;
    morningPeriod:DispoPeriodTime=new DispoPeriodTime(new EntityID());
    eveningPeriod:DispoPeriodTime=new DispoPeriodTime(new EntityID());
    constructor(day:number=1)
    {
        this.day=day;
        this.morningPeriod.period="08h00-12h00";
        this.eveningPeriod.period="13h00-17h00";
    }
    isAvailable():boolean
    {
        return this.morningPeriod.isAvailable() && this.eveningPeriod.isAvailable();
    }
    setAllStatut(statut:StatutPeriod):void
    {
        this.morningPeriod.setStatut(statut);
        this.eveningPeriod.setStatut(statut);
    }
    toString():any
    {
        let result={};
        result[this.day]={
            'evening':this.eveningPeriod.toString(),
            'morning':this.morningPeriod.toString()
        };
        return result;
    }
    hydrate(obj:Record<string,any>):void
    {
        this.day=parseInt(Object.keys(obj)[0]);
        this.morningPeriod.hydrate(obj[this.day].morning);
        this.eveningPeriod.hydrate(obj[this.day].evening);

    }
}


export class DispoPeriodMonth
{
    monthList:string[]=["janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre"];
    currentMont:string;
    dispo:Record<number,DispoPeriodDays>={};
    constructor(month:string,dispo:Record<number,DispoPeriodDays>={})
    {
        this.currentMont=month;
        this.dispo=dispo;
    }
    toString():any
    {
        let result={};
        for(let k in this.dispo)
        {
            result[this.currentMont]={...result[this.currentMont],...this.dispo[k].toString()}
        }
        return result;
    }
    hydrate(obj)
    {
        let dispoDay:Record<number,DispoPeriodDays>={};
        for(let dayKey in obj[Object.keys(obj)[0]])
        {
            let ny={};
            ny[dayKey]={...obj[Object.keys(obj)[0]][dayKey]};
            dispoDay[dayKey]=new DispoPeriodDays(parseInt(Object.keys(obj)[0]));
            dispoDay[dayKey].hydrate(ny);
        }
        this.dispo=dispoDay;
    }
}


export class DispoPeriodYear
{
    currentYear:number;
    dispo:Record<string,DispoPeriodMonth>={};
    constructor(year:number,dispo:Record<string,DispoPeriodMonth>={})
    {
        this.currentYear=year;
        this.dispo=dispo;
    }
    toString():any
    {
        let result={};
        result[this.currentYear]={};
        for(let k in this.dispo)
        {
            result[this.currentYear]={...result[this.currentYear],...this.dispo[k].toString()}
        }
        return result;
    }
    hydrate(obj:any):void
    {
        let dispoMonth:Record<string,DispoPeriodMonth>={};
        for(let monthKey in obj[Object.keys(obj)[0]])
        {
            let ny={};
            ny[monthKey]={...obj[Object.keys(obj)[0]][monthKey]};
            dispoMonth[monthKey]=new DispoPeriodMonth(Object.keys(obj)[0])
            dispoMonth[monthKey].hydrate(ny);
        }
        this.dispo=dispoMonth;
    }
}

export class Disponibilite
{
    dispo:Record<string,DispoPeriodYear>={};
    constructor(dispo:Record<string,DispoPeriodYear>={})
    {
        this.dispo=dispo;
    }
    toString():any
    {
        let result={ };
        for(let k in this.dispo)
        {
            result={...result,...this.dispo[k].toString()}
        }
        return result;
    }
     hydrate(obj)
    {
        let dispoYear:Record<string,DispoPeriodYear>={};
        for(let yearKey in obj)
        {
            let ny={};
            ny[yearKey]={...obj[yearKey]};
            dispoYear[yearKey]=new DispoPeriodYear(parseInt(yearKey));
            dispoYear[yearKey].hydrate(ny);
        }
        this.dispo=dispoYear;
    }
}

export function mergeDisponibility(d1:Disponibilite,d2:Disponibilite)
{
    let disp=d1.toString();
    recurMergeDisponibility(disp,d2.toString());
    let dispoF:Disponibilite=new Disponibilite();
    dispoF.hydrate(disp);
    return dispoF;
}

function recurMergeDisponibility(objLevel,obj)
{
    if(objLevel.hasOwnProperty(Object.keys(obj)[0]))
    {
      this.recurAddDisponibility(objLevel[Object.keys(obj)[0]],obj[Object.keys(obj)[0]]);
    }
    else{
      objLevel[Object.keys(obj)[0]]=obj[Object.keys(obj)[0]];
    }
}