import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { FirebaseApi } from 'src/app/utils/vendors/google-api/firebase/FirebaseApi';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';
import { Disponibilite, mergeDisponibility } from '../entity/disponibility';
import { EntityID } from '../entity/entityid';
import { Teacher } from '../entity/teacher';

@Injectable({
    providedIn: 'root'
  })
export class TeacherService
{
    teacherList:Teacher[]=[];
    teacherSubject:Subject<Teacher[]>=new Subject<Teacher[]>();
    constructor(private firebaseApi:FirebaseApi){
        this.getListTeacherFromApi()
        .then((result:ResultStatut)=>{
            for(const key in result.result)
            {
                let tid:EntityID=new EntityID();
                tid.setId(key)
                let teacher=new Teacher(tid);
                this.teacherList.push(teacher);
            }
        })
        this.emitTeacherList();
    }

    addTeacher(teacher:Teacher)
    {
        return new Promise<ResultStatut>((resolve,reject)=>{{
            this.firebaseApi.set(`teachers`,teacher.toString())
            .then((result:ResultStatut)=> {
                this.teacherList.push(teacher);
                this.emitTeacherList();
                resolve(result)
            })
            .catch((error:ResultStatut)=>{
                this.firebaseApi.handleApiError(error);
                reject(error);
            })
        }})
    }
    isAvailable(idTeacher:String):boolean
    {
        let t=this.teacherList.find((teacher:Teacher)=>teacher.id.toString()==idTeacher);
        if(t) return true
    }
    getTeacherFromApi(idTeacher:EntityID):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
            this.firebaseApi.fetchOnce(`teachers/${idTeacher.toString()}`)
            .then((result:ResultStatut)=>resolve(result))
            .catch((error:ResultStatut)=>{
                this.firebaseApi.handleApiError(error);
                reject(error);
            })
        })
    }
    getListTeacherFromApi():Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{{
            this.firebaseApi.fetch("teachers")
            .then((result:ResultStatut)=> resolve(result))
            .catch((error:ResultStatut)=>{
                this.firebaseApi.handleApiError(error);
                reject(error);
            })
        }})
    }

    emitTeacherList()
    {
        this.teacherSubject.next(this.teacherList.slice());
    }

    addDisponibility(idTeacher:EntityID,dispo:Disponibilite):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve,reject)=>{
            let teacher=this.teacherList.find((teacher:Teacher)=>teacher.id.toString()==idTeacher.toString());
            if(teacher)
            {
                teacher.dispo=mergeDisponibility(teacher.dispo,dispo)
                this.firebaseApi.set(`teachers/${idTeacher}/disponibility`,teacher.dispo)
                .then((result:ResultStatut)=> resolve(result))
                .catch((error:ResultStatut)=>{
                    this.firebaseApi.handleApiError(error);
                    reject(error);
                })
            }
            else
            {
                this.getTeacherFromApi(idTeacher)
                .then((result:ResultStatut)=>{
                    teacher=new Teacher(new EntityID());
                    teacher.hydrate(result.result);
                    this.teacherList.push(teacher);
                    this.emitTeacherList();
                    this.addDisponibility(teacher.id,dispo);
                })
                .catch((error:ResultStatut)=>{
                    this.firebaseApi.handleApiError(error);
                    reject(error);
                })
            }
        })
        


    }
}