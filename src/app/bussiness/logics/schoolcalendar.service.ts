import { Injectable } from '@angular/core';
import { FirebaseApi } from 'src/app/utils/vendors/google-api/firebase/FirebaseApi';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';
import { Disponibilite } from '../entity/disponibility';

@Injectable({
    providedIn: 'root'
  })
export class SchoolCalendarService
{
    constructor(private firebaseApi:FirebaseApi){}
    sheduleCourse(
        school:{faculte:String,filiere:String,niveau:number},
        dispo:Disponibilite):Promise<ResultStatut>
        {
            return new Promise<ResultStatut>((resolve,reject)=>{{
                this.firebaseApi.set(`udm/${school.faculte}/${school.filiere}/${school.niveau}/calendar`,dispo.toString())
                .then((result:ResultStatut)=> resolve(result))
                .catch((error:ResultStatut)=>{
                    this.firebaseApi.handleApiError(error);
                    reject(error);
                })
            }})
        }
}